import badge
import binascii
from nick_colours import sha1
import ugfx

name = badge.nvs_get_str('owner', 'name', 'Hacker1337')

#Nice clean screen
ugfx.clear(ugfx.BLACK)
ugfx.flush()
ugfx.clear(ugfx.WHITE)
ugfx.flush()

shasum = sha1.sha1(name.encode('utf8'))
colours, extra = shasum[:36], shasum[36:]

r1, g1, b1, r2, g2, b2, r3, g3, b3, r4, g4, b4, r5, g5, b5, r6, g6, b6 = binascii.unhexlify(colours)

badge.leds_init()
badge.leds_send_data(bytes([g1, r1, b1, 0,
                            g2, r2, b2, 0,
                            g3, r3, b3, 0,
                            g4, r4, b4, 0,
                            g5, r5, b5, 0,
                            g6, r6, b6, 0]), 24)

ugfx.string_box(0,  52, 296, 24, name,        "Roboto_Black22", ugfx.BLACK, ugfx.justifyCenter)
ugfx.string_box(0, 104, 296, 24, '+' + extra, "Roboto_Black22", ugfx.BLACK, ugfx.justifyRight)
ugfx.flush()

import deepsleep
badge.eink_busy_wait()
deepsleep.start_sleeping(60000)
